import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import Services from './components/services/Services'
import Commentaire from './components/commentaire/Commentaire'
function App() {
  return (
    <div >
      
      <Services/>
      <Commentaire/>

    </div>
  );
}

export default App;
import React from 'react';
import './services.css';
import Img1 from '../../../src/image/Img1.png'
import Img2 from '../../../src/image/Img2.png'
import Img3 from '../../../src/image/Img3.png'
import Img4 from '../../../src/image/Img4.png'
import Card from '../Card/Card'

const Services = () => {
    return (
        <div className="services">
            <div className="awesome">
                <span>Tous les services</span>
                <p id='A'><span id='B'>Bin</span><span id='C'>_</span>Edk</p>
                <span id='D'>Lorem ipsum dolor sit, amet consectetur adipisicing elit
                    <br />Magni aut quas tempora facere id reprehenderit</span>
            </div>
            <div className="cards">
                <div style={{left:'4rem'}} id='cardT'>
                <Card
                    Img={Img1}
                    heading={'Nettoyage'}
                    detail={"HAYAT ABRAIK HAYAT ABRAIK HAYAT ABRAIK HAYAT ABRAIK"} />
                </div>
                <div style={{right:'-20rem', bottom:'20rem'}} id='cardB'>
                <Card
                    Img={Img2}
                    heading={'Nettoyage'}
                    detail={"HAYAT ABRAIK HAYAT ABRAIK HAYAT ABRAIK HAYAT ABRAIK"} />
                </div>
                <div style={{right:'-36rem', bottom:'30rem'}} id='cardT'>
                <Card
                    Img={Img3}
                    heading={'Nettoyage'}
                    detail={"HAYAT ABRAIK HAYAT ABRAIK HAYAT ABRAIK HAYAT ABRAIK"} />
                </div>
                <div style={{right:'-52rem', bottom:'52rem'}} id='cardB'>
                <Card
                    Img={Img4}
                    heading={'Nettoyage'}
                    detail={"HAYAT ABRAIK HAYAT ABRAIK HAYAT ABRAIK HAYAT ABRAIK"} />
                </div>
            </div>
        </div>
    )
}

export default Services;

import React from 'react';
import './commentaire.css';
import Com1 from '../../image/comA.jpg';
import Com2 from '../../image/comB.jpg';
import Com3 from '../../image/comC.jpg';
import {Pagination} from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';

const data = [
  {
    id: 1,
    image: Com1,
    title: 'Hanane Ennazih',
    subtitle: 'nettoyage',
    commentaire: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit Magni aut quas tempora facere id reprehenderit',
  },
  {
    id: 2,
    image: Com2,
    title: 'Hanane Ennazih',
    subtitle: 'nzttoyage',
    commentaire: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit Magni aut quas tempora facere id reprehenderit',
  },
  {
    id: 3,
    image: Com3,
    title: 'Hanane Ennazih',
    subtitle: 'nzttoyage',
    commentaire: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit Magni aut quas tempora facere id reprehenderit',
  },
];

const Commentaire = () => {
  return (
    <section className="testimonials container section">
      <h2 className="section_title">Client & reviews</h2>
      <Swiper className="testimonials_container grd"
       modules={[Pagination]}
       spaceBetween={30}
       slidesPerView={1}
       loop={true}
       grabCursor={true}
       pagination={{ clickable: true }}
       >
        {data.map(({ id, image, title, subtitle, commentaire }) => {
          return (
            <SwiperSlide className="testimonials_item" key={id}>
              <div className="thumb">
                <img src={image} alt="" style={{width: '95px'}} className='rounded-pill'/>
              </div>
              <h3 className="testimonials_title">{title}</h3>
              <h5 className="subtitle">{subtitle}</h5>
              <div className="comment">{commentaire}</div>
            </SwiperSlide>
          );
        })}
      </Swiper>
    </section>
  );
};

export default Commentaire;
